VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "屏蔽Chrome停用非官方扩展程序的提示"
   ClientHeight    =   2340
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   4305
   DrawMode        =   4  'Mask Not Pen
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   4305
   StartUpPosition =   2  '屏幕中心
   Begin VB.OptionButton Option2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Microsoft Edge"
      ForeColor       =   &H80000008&
      Height          =   230
      Left            =   2310
      TabIndex        =   1
      Top             =   315
      Width           =   1695
   End
   Begin VB.OptionButton Option1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Google Chrome"
      ForeColor       =   &H80000008&
      Height          =   230
      Left            =   515
      TabIndex        =   0
      Top             =   315
      Width           =   1485
   End
   Begin VB.CheckBox Check1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "备份原文件"
      ForeColor       =   &H80000008&
      Height          =   230
      Left            =   535
      TabIndex        =   2
      Top             =   840
      Value           =   1  'Checked
      Width           =   1380
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Caption         =   "点击执行补丁"
      Height          =   540
      Left            =   525
      TabIndex        =   3
      Top             =   1365
      Width           =   3270
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'博客：https://www.yangdx.com/2019/11/66.html
'文件：chrome.dll
'搜索内容：01 FF 50 08 84 C0 74 72 48 8B 0E 48
'替换内容：01 FF 50 08 84 C0 EB 72 48 8B 0E 48

Public rawFileName As String
Const chunkSize As Integer = 10 * 1024
Const putHex As Byte = &HEB

Dim rawHex(11) As Byte

Private Sub Form_Load()
    rawFileName = "chrome.dll"
    Option1.Value = True

    rawHex(0) = &H1
    rawHex(1) = &HFF
    rawHex(2) = &H50
    rawHex(3) = &H8
    rawHex(4) = &H84
    rawHex(5) = &HC0
    rawHex(6) = &H74 '这个位置打补丁
    rawHex(7) = &H72
    rawHex(8) = &H48
    rawHex(9) = &H8B
    rawHex(10) = &HE
    rawHex(11) = &H48
End Sub

Private Sub Command1_Click()
    Dim rawFilePath As String
    Dim fileSize As Long
    Dim chunk(chunkSize - 1) As Byte
    Dim pos As Long
    Dim i As Integer
    Dim j As Integer
    Dim flag As Integer
    Dim bakFilePath As String
    Dim comdlg As New Class1
    Dim selectFileName As String
    
    rawFilePath = App.Path & "\" & rawFileName
    
    If Dir(rawFilePath) = "" Then
        If MsgBox("当前目录未找到文件 " & rawFileName & "，是否手动浏览？", vbYesNo) = vbYes Then
            With comdlg
                .Filter = "补丁文件(" & rawFileName & ")" & Chr(0) & rawFileName
                .ShowOpen (Me.hWnd)
                
                selectFileName = .FileName
            End With
            
            If selectFileName <> "" Then
                rawFilePath = selectFileName
            Else
                Exit Sub
            End If
        Else
            Exit Sub
        End If
    End If
    
    On Error GoTo OnError
    
    Command1.Caption = "正在查找特征字符..."
    Command1.Enabled = False
    Check1.Enabled = False
    
    If Check1.Value = 1 Then
        bakFilePath = rawFilePath & "." & Format(Now, "yyyymmddhhmmss")
        FileCopy rawFilePath, bakFilePath
    End If
    
    fileSize = FileLen(rawFilePath)
    pos = 1
    
    Open rawFilePath For Binary As #1
    
    Do While pos < fileSize
        Get #1, pos, chunk
        
        For i = 0 To chunkSize - 1 - 12
            For j = 0 To 11
                If j = 6 Then
                    If chunk(i + j) <> rawHex(j) And chunk(i + j) <> putHex Then
                        i = i + j
                        Exit For
                    End If
                ElseIf chunk(i + j) <> rawHex(j) Then
                    i = i + j
                    Exit For
                End If
            Next j
            
            If j = 12 Then
                If chunk(i + 6) = putHex Then
                    flag = 2
                Else
                    flag = 1
                    Put #1, pos + i + 6, putHex
                End If
                
                Exit Do
            End If
            
        Next i
        
        pos = pos + chunkSize - 12
    Loop
    
    Close #1
    
    If flag = 0 Then
        If Check1.Value = 1 And Dir(bakFilePath) <> "" Then Kill bakFilePath
        MsgBox "未匹配到特征字符", vbExclamation, "失败"
    ElseIf flag = 1 Then
        MsgBox "补丁成功！", vbInformation, "恭喜"
    Else
        If Check1.Value = 1 And Dir(bakFilePath) <> "" Then Kill bakFilePath
        MsgBox "已经打过补丁了", vbInformation, "提示"
    End If
    
    Command1.Caption = "执行补丁"
    Command1.Enabled = True
    Check1.Enabled = True
    
    Exit Sub
    
OnError:
    Close #1
    
    If Check1.Value = 1 And Dir(bakFilePath) <> "" Then Kill bakFilePath
    
    Dim msg As String
    msg = "补丁失败：" & Err.Description & vbCrLf & vbCrLf & "请按以下方法解决：" & vbCrLf & "①关闭正在运行的浏览器" & vbCrLf & "②以管理身份运行补丁程序"
    MsgBox msg, vbExclamation, "失败"
    
    Command1.Caption = "执行补丁"
    Command1.Enabled = True
    Check1.Enabled = True
    
End Sub

Private Sub Option1_Click()
    rawFileName = "chrome.dll"
    Option2.Value = False
End Sub

Private Sub Option2_Click()
    rawFileName = "msedge.dll"
    Option1.Value = False
End Sub
