-----

**此仓库已停更，推荐使用 github 上另一个补丁：[Chrome-Developer-Mode-Extension-Warning-Patcher](https://github.com/Ceiridge/Chrome-Developer-Mode-Extension-Warning-Patcher)**

-----

# 程序介绍

屏蔽 Chrome 浏览器的 **『请停用以开发者模式运行的扩展程序』** 提示框。本补丁程序使用 vb6 开发，兼容 Edge 浏览器。

## 版本支持
2020/3/23 测试，目前支持到：
- Google Chrome 80.0.3987.149（64位）
- Microsoft Edge 80.0.361.69（64位）

## 使用方法
下载文件 **disable-chrome-extension-prompt.exe**，以管理员身份运行，点击补丁按钮后选择 Chrome 安装目录下的 chrome.dll 文件，或 edge 安装目录下的 msedge.dll 文件。  
补丁时可选择是否备份原文件，备份文件的名称以日期时间作为后缀，如：`chrome.dll.20200323101410` 

Chrome 的安装目录，如：`C:\Program Files (x86)\Google\Chrome\Application\80.0.3987.149`  
Edge 的安装目录，如：`C:\Program Files (x86)\Microsoft\Edge\Application\80.0.361.69`  

## 补丁原理
参考博文 https://www.yangdx.com/2019/11/66.html